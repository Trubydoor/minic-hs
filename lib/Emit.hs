module Emit where
import qualified LLVM.Context                  as LLVM
import qualified LLVM.Module
import qualified LLVM.AST                      as LLVM
import qualified LLVM.AST.Constant             as Const
import qualified LLVM.AST.Float                as FP
import           Control.Monad.Except           ( ExceptT
                                                , runExceptT
                                                )
import           Codegen
import           Syntax
import           Parser                  hiding ( call )
import           Text.Megaparsec                ( errorBundlePretty )
import           Control.Lens            hiding ( assign )
import           Data.Text               hiding ( last )
import           Data.Text.Encoding             ( decodeUtf8 )
import           Data.Map                       ( Map )
import qualified Data.Map                      as Map
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.State
import           Types
import           Data.Maybe                     ( fromMaybe )
import qualified Data.Text.IO                  as T

initModule :: LLVM.Module
initModule = emptyModule "main"

falseV :: LLVM.Operand
falseV = LLVM.ConstantOperand $ Const.Float (FP.Single 1)

process
  :: MonadIO m => LLVM.Module -> Text -> Bool -> m (Maybe (Text, LLVM.Module))
process modo source printq = do
  let res = parseToplevel source
  liftIO $ case res of
    Left  err -> putStrLn (errorBundlePretty err) >> return Nothing
    Right ex  -> do
      r <- codegen modo ex
      if printq then T.putStrLn (fst r) else pure ()
      return $ Just r

processFile :: Text -> IO (Maybe LLVM.Module)
processFile fname = do
  file        <- T.readFile (unpack fname)
  Just (t, m) <- process initModule file False
  T.writeFile "output.ll" t
  return $ Just m

liftError :: ExceptT String IO a -> IO a
liftError = runExceptT >=> either fail return

codegen :: LLVM.Module -> [TopExpr] -> IO (Text, LLVM.Module)
codegen modl fns = LLVM.withContext $ \context ->
  LLVM.Module.withModuleFromAST context newast $ \m -> do
    llstr <- LLVM.Module.moduleLLVMAssembly m
    return (decodeUtf8 llstr, newast)
 where
  modn   = usingStateT [] $ mapM codegenTop fns
  newast = runLLVM modl modn

codegenTop :: TopExpr -> LLVMState ()
codegenTop = \case
  Function ty ident args body -> do
    let lname = mkName ident
    at lname ?= (toLLVMTy ty, fmap fst (toSig args))
    fs <- get
    define (toLLVMTy ty) ident fnargs (bls fs)
   where
    fnargs = toSig args
    bls fs = createBlocks . execCodegen $ do
      entryBlock <- addBlock "entry"
      setBlock entryBlock
      forM_ args $ \a -> do
        let (VarDecl t v _) = a
        let name1           = v <> ".local"
        var <- alloca name1 (toLLVMTy t)
        store var . local (toLLVMTy t) . mkName $ v
        assign v var
      retptr    <- alloca "retvar" (toLLVMTy ty)
      exitBlock <- addBlock "exit"
      usingReaderT (CodegenInfo retptr exitBlock fs) (mapM_ cgen body)
      br exitBlock
      setBlock exitBlock
      retval <- load retptr
      ret retval
  Extern t ident args -> external (toLLVMTy t) ident $ toSig args

binops :: Codegen m => Map Op (Value -> Value -> m Value)
binops =
  [ (Plus   , add)
  , (Minus  , sub)
  , (Times  , mul)
  , (Divide , divid)
  , (Greater, gt)
  , (Eq     , eq)
  , (Leq    , leq)
  , (Less   , lt)
  ]

cgen :: Expr -> CodegenRet Value
cgen = \case
  FloatExpr n -> return . LLVM.ConstantOperand . Const.Float . FP.Single $ n
  IntExpr   n -> return . LLVM.ConstantOperand . Const.Int 32 . toInteger $ n
  BoolExpr n ->
    return . LLVM.ConstantOperand . Const.Int 1 $ if n then 1 else 0
  Var _ x                  -> getvar x >>= load
  Neg e                    -> cgen e >>= neg
  Not e                    -> cgen e >>= bnot
  VarDecl ty ident initVal -> do
    let lty = toLLVMTy ty
    var <- alloca ident lty
    val <- case initVal of
      Just i  -> cgen i
      Nothing -> pure $ getZero lty
    store var val
    assign ident var
    return val
  Call fn args -> do
    ftbl <- view ftab
    let (fty, argtys) =
          fromMaybe (error "no such function!") $ Map.lookup (mkName fn) ftbl
    vars <- mapM cgen args
    call (externf fty argtys (mkName fn)) vars
  BinOp Ass (Var _ var) val -> do
    a    <- getvar var
    cval <- cgen val
    store a cval
    return cval
  BinOp oper a b -> case Map.lookup oper binops of
    Just f -> do
      ca <- cgen a
      cb <- cgen b
      f ca cb
    Nothing -> error $ "No such operator " <> show oper
  If cond th el -> do
    ifthen <- addBlock "if.then"
    ifelse <- addBlock "if.else"
    ifexit <- addBlock "if.exit"

    -- entry
    condV  <- cgen cond
    cbr condV ifthen ifelse

    -- then
    setBlock ifthen
    mapM_ cgen th
    br ifexit

    -- else
    setBlock ifelse
    mapM_ cgen el
    br ifexit

    -- exit
    setBlock ifexit
    return condV
  While cond body -> do
    loopcond <- addBlock "loop.cond"
    loopbody <- addBlock "loop.body"
    loopend  <- addBlock "loop.end"
    br loopcond

    -- Condition
    setBlock loopcond
    condV <- cgen cond
    cbr condV loopbody loopend

    -- Body
    setBlock loopbody
    stmts <- mapM cgen body
    br loopcond

    -- End
    setBlock loopend
    return . last $ stmts
  Return e -> do
    rptr <- view rvar
    val  <- cgen e
    store rptr val
    exit <- view rblock
    br exit
    return val

getZero :: LLVM.Type -> LLVM.Operand
getZero (LLVM.FloatingPointType _) =
  LLVM.ConstantOperand $ Const.Float (FP.Single 0)
getZero (LLVM.IntegerType b) = LLVM.ConstantOperand $ Const.Int b 0
getZero t = error $ "Type " <> show t <> " not valid in minic"

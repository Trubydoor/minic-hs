module Lexer where
import           Types
import           Text.Megaparsec
import qualified Text.Megaparsec.Char.Lexer    as ML
import           Text.Megaparsec.Char          as C
import           Data.Int                       ( Int32 )
import           Data.Text
import           Control.Monad

ws :: MonadParser m => m ()
ws = ML.space space1 lineComment blockComment
 where
  lineComment  = ML.skipLineComment "//"
  blockComment = ML.skipBlockComment "/*" "*/"

lexeme :: MonadParser p => p a -> p a
lexeme = ML.lexeme ws

integer :: MonadParser m => m Int32
integer = lexeme ML.decimal

float :: MonadParser m => m Float
float = lexeme ML.float

symbol :: MonadParser p => Text -> p Text
symbol = ML.symbol ws

symbol_ :: MonadParser p => Text -> p ()
symbol_ = void . symbol

keyword :: MonadParser p => Text -> p ()
keyword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

keywords :: [Text]
keywords =
  ["def", "extern", "if", "then", "else", "int", "float", "true", "false"]

identifier :: MonadParser p => p Text
identifier = (lexeme . try) (p >>= check)
 where
  p =
    cons
      <$> letterChar
      <*> (pack <$> many (alphaNumChar <|> char '-' <|> char '_'))
  check x = if x `elem` keywords
    then fail $ "keyword " ++ show x ++ " cannot be an identifier"
    else return x

parens :: MonadParser p => p a -> p a
parens = between (symbol "(") (symbol ")")

semicolon :: MonadParser p => p Text
semicolon = symbol ";"

comma :: MonadParser p => p Text
comma = symbol ","

commaSep :: MonadParser p => p a -> p [a]
commaSep = (`sepBy` comma)

semiSep :: Parser a -> Parser [a]
semiSep = (`sepBy1` semicolon)

commaSepList :: MonadParser p => p a -> p [a]
commaSepList = parens . commaSep

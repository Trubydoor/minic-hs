module Syntax where
import           Types
import           Data.Int                       ( Int32 )
import           Data.Text
import           TextShow

data T = IntT | FloatT | BoolT | VoidT
  deriving (Show, Eq)

ppT :: T -> Text
ppT = \case
  IntT   -> "int"
  FloatT -> "float"
  BoolT  -> "bool"
  VoidT  -> "void"

data Expr where
  FloatExpr ::Float -> Expr
  IntExpr ::Int32 -> Expr
  BoolExpr ::Bool -> Expr
  BinOp ::Op -> Expr -> Expr -> Expr
  Var ::T -> Text -> Expr
  VarDecl ::T -> Text -> Maybe Expr -> Expr
  Call ::Name -> [Expr] -> Expr
  If ::Expr -> [Expr] -> [Expr] -> Expr
  Not ::Expr -> Expr
  Neg ::Expr -> Expr
  Return ::Expr -> Expr
  While ::Expr -> [Expr] -> Expr
  deriving (Show, Eq)

ppExpr :: Expr -> Text
ppExpr = \case
  FloatExpr f             -> showt f
  IntExpr   i             -> showt i
  BoolExpr  b             -> if b then "true" else "false"
  BinOp op a b            -> ppExpr a <> " " <> ppOp op <> " " <> ppExpr b
  Var _ name              -> name
  VarDecl t name (Just e) -> ppT t <> " " <> name <> " = " <> ppExpr e <> ";"
  VarDecl t name Nothing  -> ppT t <> " " <> name <> ";"
  Call name exprs -> name <> "(" <> intercalate "," (fmap ppExpr exprs) <> ")"
  If cond thenb elseb ->
    "if ("
      <> ppExpr cond
      <> ") {\n"
      <> intercalate ";\n" (fmap ppExpr thenb)
      <> ";\n"
      <> "} else {\n"
      <> intercalate ";\n" (fmap ppExpr elseb)
      <> ";\n"
      <> "}"
  Not    expr -> "!(" <> ppExpr expr <> ")"
  Neg    expr -> "-(" <> ppExpr expr <> ")"
  Return expr -> "return " <> ppExpr expr <> ";"
  While cond block ->
    "while ("
      <> ppExpr cond
      <> ") {\n"
      <> intercalate ";\n" (fmap ppExpr block)
      <> ";\n}"

data TopExpr where
  Function ::T -> Name -> [Expr] -> [Expr] -> TopExpr
  Extern ::T -> Name -> [Expr] -> TopExpr
  deriving Show

ppOp :: Op -> Text
ppOp = \case
  Ass     -> "="
  Or      -> "||"
  And     -> "&&"
  Eq      -> "=="
  Neq     -> "!="
  Leq     -> "<="
  Less    -> "<"
  Geq     -> ">="
  Greater -> ">"
  Plus    -> "+"
  Minus   -> "-"
  Times   -> "*"
  Divide  -> "/"
  Mod     -> "%"

data Op
  = Ass
  | Or
  | And
  | Eq
  | Neq
  | Leq
  | Less
  | Geq
  | Greater
  | Plus
  | Minus
  | Times
  | Divide
  | Mod
  deriving (Eq, Ord, Show)

module Parser where

import           Control.Monad.Combinators.Expr
import           Text.Megaparsec
import           Control.Lens
import           Control.Monad.State
import           Data.Map                       ( Map )
import           Data.Text
import           Data.Functor

import           Lexer
import           Syntax
import           Types

type SymTab = Map Text T
type ParserState = SymTab

int :: MonadParser m => m Expr
int = integer <&> IntExpr

floating :: MonadParser m => m Expr
floating = float <&> FloatExpr

literal :: MonadParser m => m Expr
literal = bool <|> try floating <|> int

bool :: MonadParser m => m Expr
bool = true <|> false
 where
  true  = keyword "true" $> BoolExpr True
  false = keyword "false" $> BoolExpr False

binary :: MonadParser m => Op -> Operator m Expr
binary f = InfixL $ symbol (ppOp f) $> BinOp f

table :: MonadParser m => [[Operator m Expr]]
table =
  [ [Prefix $ symbol "!" $> Not, Prefix $ symbol "-" $> Neg]
  , [binary Times, binary Divide, binary Mod]
  , [binary Plus, binary Minus]
  , [ binary Geq
    , binary Greater
    , binary Leq
    , binary Less
    , binary Neq
    , binary Eq
    , binary And
    , binary Or
    ]
  , [binary Ass]
  ]

expr :: (MonadState ParserState m, MonadParser m) => m Expr
expr = makeExprParser factor table

notExpr :: (MonadState ParserState m, MonadParser m) => m Expr
notExpr = do
  keyword "!"
  Not <$> expr

negExpr :: (MonadState ParserState m, MonadParser m) => m Expr
negExpr = do
  keyword "-"
  Neg <$> expr

varDecl :: (MonadState ParserState m, MonadParser m) => m (T, Text)
varDecl = do
  ty   <- def
  name <- identifier
  at name ?= ty
  return (ty, name)

varDeclStmt :: (MonadState ParserState m, MonadParser m) => m Expr
varDeclStmt = do
  (ty, name) <- varDecl
  return $ VarDecl ty name Nothing

varDeclExpr :: (MonadState ParserState m, MonadParser m) => m Expr
varDeclExpr = do
  (ty, name) <- varDecl
  e          <- optional $ (keyword "=") *> expr
  return $ VarDecl ty name e

variable :: (MonadState ParserState m, MonadParser m) => m Expr
variable = do
  name <- identifier
  s    <- use $ at name
  ty   <- case s of
    Just t  -> return t
    Nothing -> fail $ "No known variable " <> unpack name
  return $ Var ty name

intDef :: MonadParser m => m T
intDef = keyword "int" $> IntT

floatDef :: MonadParser m => m T
floatDef = keyword "float" $> FloatT

boolDef :: MonadParser m => m T
boolDef = keyword "bool" $> BoolT

def :: MonadParser m => m T
def = boolDef <|> intDef <|> floatDef

function :: MonadParser m => m TopExpr
function =
  usingStateT []
    $   Function
    <$> def
    <*> identifier
    <*> commaSepList varDeclStmt
    <*> block

extern :: MonadParser m => m TopExpr
extern = do
  keyword "extern"
  t    <- def
  name <- identifier
  args <- evalStateT (commaSepList varDeclStmt) []
  symbol_ ";"
  return $ Extern t name args

call :: (MonadState ParserState m, MonadParser m) => m Expr
call = Call <$> identifier <*> commaSepList expr

factor :: (MonadState ParserState m, MonadParser m) => m Expr
factor = try retExpr <|> try literal <|> try call <|> parens expr <|> variable

defn :: MonadParser m => m TopExpr
defn = function <|> extern

contents :: MonadParser p => p a -> p a
contents = between ws eof

toplevel :: MonadParser m => m [TopExpr]
toplevel = many defn

execParser :: Parser a -> Text -> Either ParseE a
execParser parser = parse (contents parser) ""

parseToplevel :: Text -> Either ParseE [TopExpr]
parseToplevel = execParser toplevel

ifThen :: (MonadState ParserState m, MonadParser m) => m Expr
ifThen = do
  keyword "if"
  condV <- parens expr
  th    <- block
  keyword "else"
  If condV th <$> block

while :: (MonadState ParserState m, MonadParser m) => m Expr
while = do
  keyword "while"
  While <$> parens expr <*> block

stmt :: (MonadState ParserState m, MonadParser m) => m Expr
stmt = ifThen <|> try while <|> semiStmt
  where semiStmt = (varDeclExpr <|> expr) <* semicolon

block :: (MonadState ParserState m, MonadParser m) => m [Expr]
block = symbol_ "{" *> many stmt <* symbol_ "}"

retExpr :: (MonadState ParserState m, MonadParser m) => m Expr
retExpr = keyword "return" *> (Return <$> expr)

{-# LANGUAGE TemplateHaskell, TypeSynonymInstances #-}
module Codegen where

import Data.ByteString.Short (toShort, ShortByteString)
import LLVM.AST (Named((:=)))
import qualified LLVM.AST as LLVM
import qualified LLVM.AST.Constant as Const
import qualified LLVM.AST.Global as LLVM
import qualified LLVM.AST.Linkage as LLVM
import qualified LLVM.AST.Attribute as LLVM
import qualified LLVM.AST.CallingConvention as LLVM.CC
import qualified LLVM.AST.FloatingPointPredicate as FP
import qualified LLVM.AST.IntegerPredicate as IP
import qualified LLVM.AST.Float as FP
import qualified LLVM.AST.Type as LLVM
import LLVM.AST.Typed (typeOf)
import Control.Lens.Operators
import Control.Lens.TH
import Control.Lens
import Syntax
import qualified Data.Map as Map
import Data.Map (Map)
import Data.Text (Text, pack, unpack)
import Data.Text.Encoding (encodeUtf8)
import Control.Monad.State
import Control.Monad.Reader hiding (local)
import Data.List (sortBy)
import Data.Function (on)
import qualified LLVM.AST.AddrSpace            as LLVM

floatT :: LLVM.Type
floatT = LLVM.float

boolT :: LLVM.Type
boolT = LLVM.i1

intT :: LLVM.Type
intT = LLVM.i32

mkName :: Text -> LLVM.Name
mkName = LLVM.mkName . unpack

type Value = LLVM.Operand

type FunctionTable = Map LLVM.Name (LLVM.Type, [LLVM.Type])
type SymbolTable = Map Text LLVM.Operand
type Names = Map ShortByteString Int

uniqueName :: Text -> Names -> (Text, Names)
uniqueName nm ns =
  let nmb = toShort $ encodeUtf8 nm
  in  case Map.lookup nmb ns of
        Nothing   -> (nm, Map.insert nmb 1 ns)
        Just indx -> (nm <> pack (show indx), Map.insert nmb (indx + 1) ns)

data BlockState = BlockState
  { _idx :: Int
  , _stack :: [LLVM.Named LLVM.Instruction]
  , _term :: Maybe (LLVM.Named LLVM.Terminator)
  } deriving (Show, Eq)


makeLenses ''BlockState

instance Ord BlockState where
  compare x y = (x ^. idx) `compare` (y ^. idx)

data CodegenState = CodegenState
  { _currentBlock :: LLVM.Name
  , _blocks :: Map LLVM.Name BlockState
  , _symtab :: SymbolTable
  , _blockCount :: Int
  , _count :: Word
  , _names :: Names
  } deriving Show

data CodegenInfo = CodegenInfo
  { _rvar :: Value
  , _rblock :: LLVM.Name
  , _ftab :: FunctionTable
  }

makeLenses ''CodegenState
makeLenses ''CodegenInfo

sortBlocks :: [(LLVM.Name, BlockState)] -> [(LLVM.Name, BlockState)]
sortBlocks = sortBy (compare `on` snd)

toLLVMTy :: T -> LLVM.Type
toLLVMTy = \case
  IntT   -> intT
  FloatT -> floatT
  BoolT  -> boolT
  VoidT  -> LLVM.VoidType

toSig :: [Expr] -> [(LLVM.Type, LLVM.Name)]
toSig = map (\(VarDecl t v _) -> (toLLVMTy t, mkName v))

createBlocks :: CodegenState -> [LLVM.BasicBlock]
createBlocks m = map makeBlock $ sortBlocks $ itoList (_blocks m)

makeBlock :: (LLVM.Name, BlockState) -> LLVM.BasicBlock
makeBlock (l, BlockState _ s t) = LLVM.BasicBlock l (reverse s) (maketerm t)
 where
  maketerm (Just x) = x
  maketerm Nothing  = error $ "Block has no terminator: " <> show l

emptyCodegenState :: CodegenState
emptyCodegenState = CodegenState (LLVM.Name "entry") [] [] 1 0 []

execCodegen :: CodegenTop a -> CodegenState
execCodegen m = execState (runCodegen m) emptyCodegenState

newtype CodegenTop a = CodegenTop { runCodegen :: State CodegenState a }
  deriving (Functor, Applicative, Monad, MonadState CodegenState)

type CodegenRet a = ReaderT CodegenInfo CodegenTop a

type Codegen m = (MonadState CodegenState m)

newtype LLVM a = LLVM (State LLVM.Module a)
  deriving (Functor, Applicative, Monad, MonadState LLVM.Module)

type LLVMState a = StateT FunctionTable LLVM a

makeLensesWith camelCaseFields ''LLVM.Module

runLLVM :: LLVM.Module -> LLVM a -> LLVM.Module
runLLVM modl (LLVM m) = execState m modl

getModuleDefinitions :: LLVM [LLVM.Definition]
getModuleDefinitions = use definitions

emptyModule :: Text -> LLVM.Module
emptyModule label =
  LLVM.defaultModule { LLVM.moduleName = toShort $ encodeUtf8 label }

addDefn :: LLVM.Definition -> LLVM ()
addDefn d = definitions <>= [d]

define
  :: LLVM.Type
  -> Text
  -> [(LLVM.Type, LLVM.Name)]
  -> [LLVM.BasicBlock]
  -> LLVMState ()
define retty label argtys body = do
  let lname = mkName label
  at lname ?= (retty, map fst argtys)
  lift $ addDefn $ LLVM.GlobalDefinition $ LLVM.functionDefaults
    { LLVM.name        = mkName label
    , LLVM.parameters  = ( [ LLVM.Parameter ty nm [] | (ty, nm) <- argtys ]
                         , False
                         )
    , LLVM.returnType  = retty
    , LLVM.basicBlocks = body
    }

external :: LLVM.Type -> Text -> [(LLVM.Type, LLVM.Name)] -> LLVMState ()
external retty label argtys = do
  let lname = mkName label
  at lname ?= (retty, map fst argtys)
  lift $ addDefn $ LLVM.GlobalDefinition $ LLVM.functionDefaults
    { LLVM.name        = LLVM.Name $ toShort $ encodeUtf8 label
    , LLVM.linkage     = LLVM.External
    , LLVM.parameters  = ( [ LLVM.Parameter ty nm [] | (ty, nm) <- argtys ]
                         , False
                         )
    , LLVM.returnType  = retty
    , LLVM.basicBlocks = []
    }

entry :: Codegen m => m LLVM.Name
entry = use currentBlock

emptyBlock :: Int -> BlockState
emptyBlock i = BlockState i [] Nothing

addBlock :: Codegen m => Text -> m LLVM.Name
addBlock bname = do
  indx <- use blockCount
  nms  <- use names

  let new             = emptyBlock indx
  let (qname, supply) = uniqueName bname nms

  blocks . at (mkName qname) .= Just new
  blockCount += 1
  names .= supply
  return $ mkName qname

setBlock :: Codegen m => LLVM.Name -> m ()
setBlock = (currentBlock .=)

getBlock :: Codegen m => m LLVM.Name
getBlock = use currentBlock

modifyBlock :: Codegen m => BlockState -> m ()
modifyBlock new = do
  active <- use currentBlock
  blocks . at active .= Just new

fresh :: Codegen m => m Word
fresh = count <+= 1

freshName :: Codegen m => Text -> m LLVM.Name
freshName txt = do
  n <- fresh
  return . LLVM.mkName $ unpack txt <> show n

current :: Codegen m => m BlockState
current = do
  c    <- use currentBlock
  look <- use $ blocks . at c
  case look of
    Just x  -> return x
    Nothing -> error $ "No such block: " <> show c

assign :: Codegen m => Text -> Value -> m ()
assign var x = symtab . at var ?= x

getvar :: Codegen m => Text -> m Value
getvar var = do
  look <- use $ symtab . at var
  case look of
    Just x  -> return x
    Nothing -> error $ "Local variable not in scope: " <> show var

local :: LLVM.Type -> LLVM.Name -> Value
local = LLVM.LocalReference 

externf :: LLVM.Type -> [LLVM.Type] -> LLVM.Name -> LLVM.Operand
externf ty tys nm = LLVM.ConstantOperand $ Const.GlobalReference
  (LLVM.PointerType (LLVM.FunctionType ty tys False) (LLVM.AddrSpace 0))
  nm

instr :: Codegen m => Text -> LLVM.Type -> LLVM.Instruction -> m Value
instr ref ty ins = do
  ident <- freshName ref
  blk  <- current
  let i = _stack blk
  modifyBlock (blk { _stack = ident := ins : i })
  return $ local ty ident

voidInstr :: Codegen m => LLVM.Instruction -> m ()
voidInstr ins = do
  let voidinst = LLVM.Do ins
  blk <- current
  let i = _stack blk
  modifyBlock (blk { _stack = voidinst : i })
  return ()

terminator :: Codegen m => LLVM.Named LLVM.Terminator -> m ()
terminator trm = do
  blk <- current
  modifyBlock (blk { _term = Just trm })

intToFloat :: Codegen m => LLVM.Operand -> m Value
intToFloat a = instr "ftmp" floatT $ LLVM.SIToFP a floatT []

add :: Codegen m => Value -> Value -> m Value
add a b
  | (t1, t2) == (intT, intT) = instr "add" intT $ LLVM.Add False False a b []
  | (t1, t2) == (floatT, floatT) = instr "add" floatT
  $ LLVM.FAdd LLVM.noFastMathFlags a b []
  | (t1, t2) == (intT, floatT) = do
      aP <- intToFloat a
      add aP a
  | (t1, t2) == (floatT, intT) = add b a
  | otherwise = error "Mismatched types to +"
  where t1 = typeOf a
        t2 = typeOf b

sub :: Codegen m => Value -> Value -> m Value
sub a b
  | (t1, t2) == (intT, intT) = instr "sub" intT $ LLVM.Sub False False a b []
  | (t1, t2) == (floatT, floatT) = instr "sub" floatT
  $ LLVM.FSub LLVM.noFastMathFlags a b []
  | (t1, t2) == (intT, floatT) = do
      aP <- intToFloat a
      sub aP b
  | (t1, t2) == (floatT, intT) = do
      bP <- intToFloat b
      sub a bP
  | otherwise = error "Mismatched types to -"
  where t1 = typeOf a
        t2 = typeOf b

mul :: Codegen m => Value -> Value -> m Value
mul a b
  | (t1, t2) == (intT, intT) = instr "mul" intT $ LLVM.Mul False False a b []
  | (t1, t2) == (floatT, floatT) = instr "mul" floatT
  $ LLVM.FMul LLVM.noFastMathFlags a b []
  | (t1, t2) == (intT, floatT) = do
      aP <- intToFloat a
      mul aP b
  | (t1, t2) == (floatT, intT) = mul b a
  | otherwise = error "Mismatched types to *"
  where t1 = typeOf a
        t2 = typeOf b

divid :: Codegen m => Value -> Value -> m Value
divid a b
  | (t1, t2) == (intT, intT) = instr "div" intT $ LLVM.SDiv False a b []
  | (t1, t2) == (floatT, floatT) = instr "div" floatT
  $ LLVM.FDiv LLVM.noFastMathFlags a b []
  | (t1, t2) == (intT, floatT) = do
      aP <- intToFloat a
      divid aP b
  | (t1, t2) == (floatT, intT) = do
      bP <- intToFloat b
      divid a bP
  | otherwise = error "Mismatched types to /"
  where t1 = typeOf a
        t2 = typeOf b


cmp :: Codegen m => IP.IntegerPredicate -> FP.FloatingPointPredicate -> Value -> Value -> m Value
cmp ip fp a1 a2
  | (t1, t2) == (intT, intT) = instr "cmp" intT $ LLVM.ICmp ip a1 a2 []
  | (t1, t2) == (floatT, floatT) = instr "cmp" floatT $ LLVM.FCmp fp a1 a2 []
  | (t1, t2) == (floatT, intT) = do
      aP <- intToFloat a2
      cmp ip fp aP a2
  | (t1, t2) == (intT, floatT) = do
      bP <- intToFloat a1
      cmp ip fp a1 bP
  | otherwise = error "Mismatched types to cmp"
  where t1 = typeOf a1
        t2 = typeOf a2

gt :: Codegen m => Value -> Value -> m Value
gt = cmp IP.SGT FP.OGT

eq :: Codegen m => Value -> Value -> m Value
eq = cmp IP.EQ FP.OEQ

leq :: Codegen m => Value -> Value -> m Value
leq = cmp IP.SLE FP.OLE

lt :: Codegen m => Value -> Value -> m Value
lt = cmp IP.SLT FP.OLT

neg :: Codegen m => Value -> m Value
neg a = case ty of
  LLVM.IntegerType _ -> instr "neg" ty $ LLVM.Sub False False (LLVM.ConstantOperand $ Const.Int 32 0) a []
  LLVM.FloatingPointType _ -> instr "neg" floatT $ LLVM.FSub LLVM.noFastMathFlags (LLVM.ConstantOperand $ Const.Float (FP.Single 0)) a []
  _ -> error "Invalid type arguments to -"
  where ty = typeOf a

br :: Codegen m => LLVM.Name -> m ()
br var = terminator $ LLVM.Do $ LLVM.Br var []

cbr :: Codegen m => Value -> LLVM.Name -> LLVM.Name -> m ()
cbr cond tr fl = terminator $ LLVM.Do $ LLVM.CondBr cond tr fl []

ret :: Codegen m => LLVM.Operand -> m ()
ret val = terminator $ LLVM.Do $ LLVM.Ret (Just val) []

toArgs :: [Value] -> [(LLVM.Operand, [LLVM.ParameterAttribute])]
toArgs = map (, [])

call :: Codegen m => Value -> [Value] -> m Value
call fn args = instr "call" (typeOf fn) $ LLVM.Call Nothing LLVM.CC.GHC [] (Right fn) (toArgs args) [] []

alloca :: Codegen m => Text -> LLVM.Type -> m Value
alloca ref ty = instr ref ty $ LLVM.Alloca ty Nothing 0 []

store :: Codegen m => Value -> Value -> m ()
store ptr val
  | t1 == t2 = voidInstr $ LLVM.Store False ptr val Nothing 0 []
  | otherwise = error $ "Attempt to store value of type " ++ show t2 ++ " in variable of type " ++ show t1
  where t1 = typeOf ptr
        t2 = typeOf val

load :: Codegen m => Value -> m Value
load ptr = instr "load" ty $ LLVM.Load False ptr Nothing 0 []
  where ty = typeOf ptr

fcmp :: Codegen m => FP.FloatingPointPredicate -> Value -> Value -> m Value
fcmp cond a b = instr "fcmp" boolT $ LLVM.FCmp cond a b []

bnot :: Codegen m => Value -> m Value
bnot a = instr "not" (typeOf a) $ LLVM.Xor (LLVM.ConstantOperand $ Const.Int 1 1) a []

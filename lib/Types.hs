module Types where
import           Text.Megaparsec                ( Parsec
                                                , ParsecT
                                                , ParseErrorBundle
                                                , MonadParsec
                                                )
import           Data.Void                      ( Void )
import           Data.Text
import           Control.Monad.Reader
import           Control.Monad.State

type Parser = Parsec Void Text
type ParserT a = ParsecT Void Text a
type ParseE = ParseErrorBundle Text Void

type MonadParser m = MonadParsec Void Text m

type Name = Text

usingReaderT :: Monad m => s -> ReaderT s m a -> m a
usingReaderT = flip runReaderT

usingStateT :: Monad m => s -> StateT s m a -> m a
usingStateT = flip evalStateT

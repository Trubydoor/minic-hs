{-# LANGUAGE TemplateHaskell #-}
module Main where
import           Test.Tasty
import           Test.Tasty.QuickCheck
import           Test.Tasty.HUnit
import           Test.Tasty.TH
import           Parser
import           Syntax
import           Data.Int                       ( Int32 )
import           TextShow
import           Types                          ( Parser )

main :: IO ()
main = $(defaultMainGenerator)

tests :: TestTree
tests = $(testGroupGenerator)

lit_parser :: TextShow t => Parser Expr -> (t -> Expr) -> t -> Bool
lit_parser p e a = case execParser p (showt a) of
    Right e1 -> e1 == (e a)
    _        -> False

prop_int_parser :: Positive Int32 -> Bool
prop_int_parser (Positive i) = lit_parser int IntExpr i

prop_float_parser :: Positive Float -> Bool
prop_float_parser (Positive f) = lit_parser floating FloatExpr f

bool_parser :: Parser Expr -> Bool -> Assertion
bool_parser p True  = Right (BoolExpr True) @=? execParser p "true"
bool_parser p False = Right (BoolExpr False) @=? execParser p "false"

test_bool_parser :: [TestTree]
test_bool_parser =
    [ testCase "true" $ bool_parser bool True
    , testCase "false" $ bool_parser bool False
    ]

prop_literal_parser_int :: Positive Int32 -> Bool
prop_literal_parser_int (Positive i) = lit_parser literal IntExpr i

prop_literal_parser_float :: Positive Float -> Bool
prop_literal_parser_float (Positive f) = lit_parser literal FloatExpr f

test_literal_parser_bool :: [TestTree]
test_literal_parser_bool =
    [ testCase "true" $ bool_parser literal True
    , testCase "false" $ bool_parser literal False
    ]

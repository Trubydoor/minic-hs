module Main where

import           System.Console.Haskeline
import           Emit
import           Data.Text
import           System.Environment
import           Control.Monad

repl :: IO ()
repl = runInputT defaultSettings (loop initModule)
 where
  loop modl = getInputLine "ready> " >>= \case
    Nothing    -> outputStrLn "Goodbye."
    Just input -> do
      modn <- process modl (pack input) True
      case modn of
        Just (_, imod) -> loop imod
        Nothing        -> loop modl

main :: IO ()
main = getArgs >>= \case
  []        -> repl
  fname : _ -> void $ processFile $ pack fname

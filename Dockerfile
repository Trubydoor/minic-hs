FROM fpco/stack-build:lts-12.11

MAINTAINER David Truby

RUN echo "deb http://apt.llvm.org/xenial llvm-toolchain-xenial-7 main" >> \
         /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y llvm-7

ENV PATH=/usr/lib/llvm-7/bin:$PATH